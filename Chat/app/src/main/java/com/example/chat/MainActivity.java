package com.example.chat;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity  {

    EditText mail, password;
    TextView tvlogin;
    Button login;
    FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseApp.initializeApp(this);
        firebaseAuth=FirebaseAuth.getInstance();

        login = (Button) findViewById(R.id.btnLogin);
        tvlogin = (TextView) findViewById(R.id.tvLogin);
        mail = (EditText) findViewById(R.id.userMail);
        password = (EditText) findViewById(R.id.userPassword);

        if(firebaseAuth.getCurrentUser()!=null){
            finish();
            Intent intent = new Intent(MainActivity.this, Logoutpage.class);
            startActivity(intent);
        }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();

            }
        });
        tvlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent_three=new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(intent_three);
            }
        });



    }
    private void userLogin() {
        String useremailid = mail.getText().toString().trim();
        String userpassword = password.getText().toString().trim();
        if (useremailid.isEmpty()) {
            Toast.makeText(MainActivity.this, "Enter EmailId", Toast.LENGTH_SHORT).show();


        } else if (userpassword.isEmpty()) {
            Toast.makeText(MainActivity.this, "Enter password", Toast.LENGTH_SHORT).show();


        }
else{
        firebaseAuth.signInWithEmailAndPassword(useremailid, userpassword).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    finish();
                    Intent intent = new Intent(MainActivity.this, Logoutpage.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(MainActivity.this, "Incorrect cridentials", Toast.LENGTH_SHORT).show();
                }

            }

        });
    }

    }

}
