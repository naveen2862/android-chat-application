package com.example.chat;

import android.content.Intent;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;

import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;




public class RegisterActivity extends AppCompatActivity {

    DatabaseReference persondatabase;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final EditText username,userpassword,usermailid;
        TextView  tvregister;

        final Button btnregister;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        firebaseAuth =FirebaseAuth.getInstance();
        persondatabase= FirebaseDatabase.getInstance().getReference("persondetails");

        username= (EditText)findViewById(R.id.etName);
        userpassword= (EditText)findViewById(R.id.etPassword);
        usermailid= (EditText)findViewById(R.id.etMailId);

        tvregister = (TextView) findViewById(R.id.tvRegister);
        btnregister=(Button)findViewById(R.id.btnRegister);


        tvregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_two=new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent_two);
            }
        });


        btnregister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                 String name,email,password;
                name=username.getText().toString().trim();
                email=usermailid.getText().toString().trim();
                password=userpassword.getText().toString().trim();

                register(name,email,password);

            }
        });




    }
    public void register(final String user_name, final String user_mailid, final String user_password){


        if(user_name.isEmpty() ||user_mailid.isEmpty()||user_password.isEmpty()){
            Toast.makeText(RegisterActivity.this,"Enter all details",Toast.LENGTH_SHORT).show();

        }

        firebaseAuth.createUserWithEmailAndPassword(user_mailid,user_password).addOnCompleteListener( new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {

                    addPerson(user_name, user_mailid, user_password);
                    Toast.makeText(RegisterActivity.this, "registration succesfull", Toast.LENGTH_SHORT).show();
                    Intent intent_four = new Intent(RegisterActivity.this, MainActivity.class);
                    startActivity(intent_four);

                }
                else{
                    Toast.makeText(RegisterActivity.this, "registration unsuccesfull", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }



    public  void addPerson(String name,String email,String password){

            String id= persondatabase.push().getKey();
            PersonAtribute artist=new PersonAtribute(id,email,password,name);
            persondatabase.child(id ).setValue(artist);
            Toast.makeText(this,"person Added",Toast.LENGTH_SHORT).show();


    }



}

