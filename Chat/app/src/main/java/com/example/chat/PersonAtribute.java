package com.example.chat;

public class PersonAtribute {

    String artistid;
    String mailid;
    String password;
    String artistname;

       PersonAtribute(String artistid, String mailid, String password, String artistname) {
        this.artistid = artistid;
        this.mailid = mailid;
        this.password = password;
        this.artistname = artistname;

    }

    public String getArtistid() {
        return artistid;
    }

    public String getMailid() {
        return mailid;
    }


    public String getPassword() {
        return password;
    }

    public String getArtistname() {
        return artistname;
    }
}
